#Образ
FROM node
#Create working directory
RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN yarn install

COPY . /app
RUN yarn test
RUN yarn build

CMD yarn start
#Порт доступа к приложению
EXPOSE 3000
